#!/bin/bash

make clean
make

cores=(1 2 4 8 16)
results=()
for core_count in ${cores[@]}; do
    results+=`./bin/sudoku $core_count`
done

echo
echo ${results[@]}
