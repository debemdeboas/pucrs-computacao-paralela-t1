CC = gcc
CFLAGS = -g -Wall -fopenmp
OBJDIR = obj
BINDIR = bin

$(BINDIR)/sudoku: $(OBJDIR)/sudoku.o
	$(CC) $(CFLAGS) -o $@ $^

$(OBJDIR)/%.o: ./%.c
	$(CC) $(CFLAGS) -c $< -o $@

.PHONY: clean
clean:
	rm -rf $(OBJDIR) $(BINDIR)

$(shell mkdir -p $(BINDIR) $(OBJDIR))
