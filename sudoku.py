
import random
import multiprocessing as mp
import datetime as dt
import sys

class SudokuBoard:
    board: list[list[int]]
    cols: int
    lines: int

    def __init__(self, cols: int = 3, lines: int = 3, initial_val: int = 0) -> None:
        cols *= 3
        lines *= 3

        self.board = SudokuBoard.new_board(cols, lines, initial_val)
        self.cols = cols
        self.lines = lines

    @staticmethod
    def new_board(m: int, n: int, initial_val: int = 0) -> list[list[int]]:
        r: list[list[int]] = []
        for _ in range(n):
            r.append([initial_val for _ in range(m)])
        return r

    def __repr__(self) -> str:
        line_width = 0
        res = ''
        line_str = ''
        for i, line in enumerate(self.board):
            for j, val in enumerate(line):
                if j % 3 == 0:
                    line_str += '|'
                line_str += f' {val} ' if val else ' . '

            if not line_width:
                line_width = len(line_str)

            if i % 3 == 0:
                res += '+'
                for _ in range(self.cols // 3):
                    res += '-' * ((line_width - 1) // 3) + '+'
                res += '\n'

            res += f'{line_str}|\n'
            line_str = ''

        res += '+'
        for _ in range(self.cols // 3):
            res += '-' * ((line_width - 1) // 3) + '+'
        res += '\n'
        return res

    def __getitem__(self, key: int) -> list[int]:
        return self.board[key]

    def size(self) -> tuple[int, int]:
        return len(self.board), len(self.board[0])

    def is_valid(self, row, col, num):
        if num in self.board[row] or \
           num in list(self.board[i][col] for i in range(self.lines)):
            return False

        row_start, col_start = 3 * (row // 3), 3 * (col // 3)
        for i in range(3):
            for j in range(3):
                if self.board[row_start + i][col_start + j] == num:
                    return False

        return True

    def generate_board(self, row = 0, col = 0, num_order: list[int] = list()):
        if row == self.lines:
            return True

        next_row, next_col = (row, col + 1) if col < self.cols - 1 else (row + 1, 0)

        if self[row][col] != 0:
            return self.generate_board(next_row, next_col)
        else:
            for num in num_order:
                if self.is_valid(row, col, num):
                    self.board[row][col] = num
                    if self.generate_board(next_row, next_col, num_order):
                        return True
                    self.board[row][col] = 0
            return False


def generate_sudoku_board(num_order) -> SudokuBoard:
    sudoku_board = SudokuBoard()
    sudoku_board.generate_board(num_order=num_order)
    return sudoku_board


def worker(seed):
    random.seed(seed)
    num_order = list(range(1, 10))
    random.shuffle(num_order)
    return generate_sudoku_board(num_order=num_order)


if __name__ == '__main__':
    choice = input(
'''Use how many cores?
[M] Use all cores (`multiprocessing.cpu_count()`)
[H] Use half of all cores
[S] Execute sequentially, remove any parallelism
[C] Use custom amount
> ''').lower()
    
    if choice == 'm':
        procs = mp.cpu_count()
    elif choice == 'h':
        procs = mp.cpu_count() // 2
    elif choice == 'c':
        try:
            procs = int(input('Number of cores: '))
        except:
            print('Invalid, exiting.')
            sys.exit(1)
    else:
        procs = 1

    random.seed(42)
    num_boards = 200 ** 2
    seeds = [random.randint(0, 2 ** 32 - 1) for _ in range(num_boards)]

    def parallel():
        with mp.Pool(procs) as p:
            results = p.map(worker, seeds)
        return results

    def sequential():
        return [worker(seed) for seed in seeds]
    
    exec_sys = parallel
    if choice == 's':
        exec_sys = sequential

    s = dt.datetime.now()
    exec_sys()
    e = dt.datetime.now()

    print(f'Done. Elapsed: {e - s}')
