#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <omp.h>

#define BOARD_SIZE 9
#define SUB_BOARD_SIZE 3
#define NUM_BOARDS (1200 * 1200)

typedef struct {
    int data[BOARD_SIZE][BOARD_SIZE];
} SudokuBoard;

bool is_valid(const SudokuBoard *board, int row, int col, int num)
{
    for (int i = 0; i < BOARD_SIZE; i++) {
        if (board->data[row][i] == num || board->data[i][col] == num) {
            return false;
        }
    }

    int row_start = SUB_BOARD_SIZE * (row / SUB_BOARD_SIZE);
    int col_start = SUB_BOARD_SIZE * (col / SUB_BOARD_SIZE);
    for (int i = 0; i < SUB_BOARD_SIZE; i++) {
        for (int j = 0; j < SUB_BOARD_SIZE; j++) {
            if (board->data[row_start + i][col_start + j] == num) {
                return false;
            }
        }
    }

    return true;
}

bool generate_board(SudokuBoard *board, int row, int col, const int num_order[BOARD_SIZE])
{
    if (row == BOARD_SIZE) {
        return true;
    }

    int next_row = col < BOARD_SIZE - 1 ? row : row + 1;
    int next_col = col < BOARD_SIZE - 1 ? col + 1 : 0;

    if (board->data[row][col] != 0) {
        return generate_board(board, next_row, next_col, num_order);
    } else {
        for (int i = 0; i < BOARD_SIZE; i++) {
            int num = num_order[i];
            if (is_valid(board, row, col, num)) {
                board->data[row][col] = num;
                if (generate_board(board, next_row, next_col, num_order)) {
                    return true;
                }
                board->data[row][col] = 0;
            }
        }
        return false;
    }
}

void worker(unsigned int seed, SudokuBoard *result)
{
    srand(seed);
    int num_order[BOARD_SIZE];
    for (int i = 0; i < BOARD_SIZE; i++) {
        num_order[i] = i + 1;
    }
    for (int i = BOARD_SIZE - 1; i > 0; i--) {
        int j = rand() % (i + 1);
        int temp = num_order[i];
        num_order[i] = num_order[j];
        num_order[j] = temp;
    }
    generate_board(result, 0, 0, num_order);
}

int save_sudoku_board(SudokuBoard *sudoku_board, const char *file_name) {
    FILE *file = fopen(file_name, "w");

    if (file == NULL) {
        printf("Error opening the file %s\n", file_name);
        return 1;
    }

    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            fprintf(file, "%d", sudoku_board->data[i][j]);
            if (j < BOARD_SIZE - 1) {
                fprintf(file, " ");
            }
        }
        fprintf(file, "\n");
    }

    fclose(file);
    return 0;
}

void print_sudoku_board(SudokuBoard board) {
    for (int i = 0; i < BOARD_SIZE; ++i) {
        if (i % SUB_BOARD_SIZE == 0) {
            printf("+---------+---------+---------+\n");
        }
        for (int j = 0; j < BOARD_SIZE; ++j) {
            if (j % SUB_BOARD_SIZE == 0) {
                printf("|");
            }
            int val = board.data[i][j];
            printf(" %d ", val);
        }
        printf("|\n");
    }
    printf("+---------+---------+---------+\n");
}

int main(int argc, char *argv[]) {
    if (argc > 1) {
        int num_threads = atoi(argv[1]);
        printf("-*- USING %d THREADS -*-\n", num_threads);
        omp_set_num_threads(num_threads);
    }

    srand(42);

    SudokuBoard *results = malloc(NUM_BOARDS * sizeof(SudokuBoard));

    if (results == NULL) {
        fprintf(stderr, "Failed to allocate memory for results array.\n");
        return 1;
    }

    unsigned int seeds[NUM_BOARDS];
    for (int i = 0; i < NUM_BOARDS; i++) {
        seeds[i] = (unsigned int)rand();
    }

    double start_time = omp_get_wtime();

    #pragma omp parallel for schedule(static)
    for (int i = 0; i < NUM_BOARDS; i++) {
        worker(seeds[i], &results[i]);
    }
    double end_time = omp_get_wtime();

    printf("Done. Elapsed: %f seconds\n", end_time - start_time);

    free(results);
    return 0;
}
